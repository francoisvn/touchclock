package touchclock.gui;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.GameCanvas;
import javax.microedition.lcdui.game.Sprite;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;

import mwt.Component;
import mwt.Font;
import mwt.midp2.ImageUtil;
import tube42.lib.ui.ImageUtils;

public class ClockDisplay extends GameCanvas implements CommandListener {

	TouchClockMIDlet midlet;
	int width, height;
	Timer timer;

	int displayState = 3; // 0: clock, 1:settings, 2:time edit, 3:intro

	double midBarPercentage = 0.1;
	double midBarOffsetPercentage = 0.4;
	double midBarButtonPercentage = 0.8;
	double lowerBarPercentage = 0.1;
	double lowerBarButtonPercentage = 0.8;
	double lowerBarSpacePercentage = 0.2;
	double mainButtonWidthPercentage = 0.9;
	double mainButtonHeightPercentage = 0.25;
	double mainButtonVertOffsetPercentage = 0.23;
	double settingSpacePercentage = 0.02;
	double settingButtonPercentage = 0.1;
	double naviButtonPercentage = 0.2;
	double naviOffsetPercentage = 0.38;
	int midBarSize;
	int midBarButtonSize;
	int midBarOffset;
	int lowerBarSize;
	int lowerBarButtonSize;
	int lowerBarSpace;
	int mainButtonWidth;
	int mainButtonHeight;
	int mainButtonVertOffset;
	int settingSpace;
	int settingButtonSize;
	int naviButtonSize;
	int naviOffset;

	int backColor = 0x363636;

	Image playImage;
	Image pauseImage;
	Image closeImage;
	Image infoImage;
	Image swapImage;
	Image clockImage;
	Image spannerImage;
	Image plusImage;
	Image minusImage;
	Image okImage;
	Image soundImage;
	Image muteImage;
	Image playImageResized;
	Image pauseImageResized;
	Image closeImageResized;
	Image infoImageResized;
	Image swapImageResized;
	Image clockImageResized;
	Image spannerImageSetting;
	Image plusImageSetting;
	Image minusImageSetting;
	Image playImageNavi;
	Image closeImageNavi;
	Image okImageNavi;
	Image soundImageResized;
	Image muteImageResized;
	Image soundImageNavi;
	Image muteImageNavi;

	Image midbarImage;
	Image buttonImage;
	Image buttonRunningImage;
	Image buttonAlertImage;
	Image midbarImageResized;
	Image buttonImageResized;
	Image buttonRunningImageResized;
	Image buttonAlertImageResized;
	Image lowbarImage;
	Image lowbarImageResized;

	double fontLargeSizeScale = 0.003;
	double fontSmallSizeScale = 0.0012;
	Font fontLarge;
	Font fontLargeWhite;
	Font fontSmall;
	char[] charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.:,;'\"()/\\*!?@+-"
			.toCharArray();
	Image[] imagesOrig = new Image[charset.length];

	int timeQuantum = 100;
	int timeLeftLower = 0;
	int timeLeftUpper = 0;
	int overtimeType = 0; // 0: absolute, 1: japanese, 2: canadian
	int overtimeUnits = 0;
	int overtimeTime = 0;
	int overtimeUnitsLower = 0;
	int overtimeUnitsUpper = 0;
	int overtimeTimeLower = 0;
	int overtimeTimeUpper = 0;
	int movesLower = 0;
	int movesUpper = 0;
	String timeNice = "";

	boolean soundOn = false;
	Player clickPlayer;
	Player beepPlayer;
	int soundMainTimeWarning = 60 * 1000 / timeQuantum;
	int soundFinalWarning = 5 * 1000 / timeQuantum;

	boolean running = false;
	boolean runningLower = true;
	boolean runningUpper = false;
	boolean someoneLost = false;

	int settingMainTime = 600;
	int settingOvertimeType = 0;
	int settingOvertimeUnits = 0;
	int settingOvertimeTime = 0;

	int editDigits = 1;
	int editWidth = 0;
	int editDigitWidth = 0;
	int editTime = 0;
	int editType = 0;

	public ClockDisplay(TouchClockMIDlet midlet) {
		super(true);
		this.midlet = midlet;

		setCommandListener(this);
		setFullScreenMode(true);

		try {
			playImage = Image.createImage("/touchclock/gui/res/play.png");
			pauseImage = Image.createImage("/touchclock/gui/res/pause.png");
			closeImage = Image.createImage("/touchclock/gui/res/close.png");
			infoImage = Image.createImage("/touchclock/gui/res/info.png");
			swapImage = Image.createImage("/touchclock/gui/res/swap.png");
			clockImage = Image.createImage("/touchclock/gui/res/clock.png");
			spannerImage = Image.createImage("/touchclock/gui/res/spanner.png");
			plusImage = Image.createImage("/touchclock/gui/res/plus.png");
			minusImage = Image.createImage("/touchclock/gui/res/minus.png");
			okImage = Image.createImage("/touchclock/gui/res/ok.png");
			soundImage = Image.createImage("/touchclock/gui/res/sound.png");
			muteImage = Image.createImage("/touchclock/gui/res/mute.png");

			midbarImage = Image.createImage("/touchclock/gui/res/midbar.png");
			buttonImage = Image.createImage("/touchclock/gui/res/button.png");
			buttonRunningImage = Image
					.createImage("/touchclock/gui/res/button_running.png");
			buttonAlertImage = Image
					.createImage("/touchclock/gui/res/button_alert.png");
			lowbarImage = Image.createImage("/touchclock/gui/res/lowbar.png");

			clickPlayer = Manager.createPlayer(
					getClass().getResourceAsStream(
							"/touchclock/gui/res/click.wav"), "audio/x-wav");
			beepPlayer = Manager.createPlayer(
					getClass().getResourceAsStream(
							"/touchclock/gui/res/beep.wav"), "audio/x-wav");
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			for (int i = 0; i < charset.length; i++) {
				imagesOrig[i] = Image.createImage("/touchclock/gui/res/font/"
						+ ((int) charset[i]) + ".png");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		updateDimensions();

		timer = new Timer();
		timer.schedule(new MyTimerTask(this), timeQuantum, timeQuantum);
	}

	private void updateDimensions() {
		int oldwidth = width;
		int oldheight = height;
		width = getWidth();
		height = getHeight();
		if (oldwidth == width && oldheight == height)
			return;

		midBarSize = (int) (midBarPercentage * height);
		midBarButtonSize = (int) (midBarButtonPercentage * midBarSize);
		midBarOffset = (int) (midBarOffsetPercentage * width);
		lowerBarSize = (int) (lowerBarPercentage * height);
		lowerBarButtonSize = (int) (lowerBarButtonPercentage * lowerBarSize);
		lowerBarSpace = (int) (lowerBarSpacePercentage * lowerBarSize);

		mainButtonWidth = (int) (mainButtonWidthPercentage * width);
		mainButtonHeight = (int) (mainButtonHeightPercentage * height);
		mainButtonVertOffset = (int) (mainButtonVertOffsetPercentage * height);

		int smalldim = width;
		if (height < smalldim)
			smalldim = height;

		settingSpace = (int) (settingSpacePercentage * smalldim);
		settingButtonSize = (int) (settingButtonPercentage * smalldim);
		naviButtonSize = (int) (naviButtonPercentage * smalldim);
		naviOffset = (int) (naviOffsetPercentage * width);

		playImageResized = ImageUtils.resize(playImage, midBarButtonSize,
				midBarButtonSize, true, true);
		playImageNavi = ImageUtils.resize(playImage, naviButtonSize,
				naviButtonSize, true, true);
		pauseImageResized = ImageUtils.resize(pauseImage, midBarButtonSize,
				midBarButtonSize, true, true);
		closeImageResized = ImageUtils.resize(closeImage, midBarButtonSize,
				midBarButtonSize, true, true);
		closeImageNavi = ImageUtils.resize(closeImage, naviButtonSize,
				naviButtonSize, true, true);
		infoImageResized = ImageUtils.resize(infoImage, midBarButtonSize,
				midBarButtonSize, true, true);
		swapImageResized = ImageUtils.resize(swapImage, lowerBarButtonSize,
				lowerBarButtonSize, true, true);
		clockImageResized = ImageUtils.resize(clockImage, lowerBarButtonSize,
				lowerBarButtonSize, true, true);
		spannerImageSetting = ImageUtils.resize(spannerImage,
				settingButtonSize, settingButtonSize, true, true);
		plusImageSetting = ImageUtils.resize(plusImage, settingButtonSize,
				settingButtonSize, true, true);
		minusImageSetting = ImageUtils.resize(minusImage, settingButtonSize,
				settingButtonSize, true, true);
		okImageNavi = ImageUtils.resize(okImage, naviButtonSize,
				naviButtonSize, true, true);
		soundImageResized = ImageUtils.resize(soundImage, lowerBarButtonSize,
				lowerBarButtonSize, true, true);
		soundImageNavi = ImageUtils.resize(soundImage, naviButtonSize,
				naviButtonSize, true, true);
		muteImageResized = ImageUtils.resize(muteImage, lowerBarButtonSize,
				lowerBarButtonSize, true, true);
		muteImageNavi = ImageUtils.resize(muteImage, naviButtonSize,
				naviButtonSize, true, true);

		midbarImageResized = ImageUtils.resize(midbarImage, width, midBarSize,
				true, true);
		lowbarImageResized = ImageUtils.resize(lowbarImage, width,
				lowerBarSize, true, true);

		buttonImageResized = ImageUtils.resize(buttonImage, mainButtonWidth,
				mainButtonHeight, true, true);
		buttonRunningImageResized = ImageUtils.resize(buttonRunningImage,
				mainButtonWidth, mainButtonHeight, true, true);
		buttonAlertImageResized = ImageUtils.resize(buttonAlertImage,
				mainButtonWidth, mainButtonHeight, true, true);

		double scaleLarge = fontLargeSizeScale * smalldim;
		double scaleSmall = fontSmallSizeScale * smalldim;
		Image[] imagesLarge = new Image[charset.length];
		Image[] imagesLargeWhite = new Image[charset.length];
		Image[] imagesSmall = new Image[charset.length];
		for (int i = 0; i < charset.length; i++) {
			imagesLargeWhite[i] = ImageUtils.resize(imagesOrig[i],
					(int) (imagesOrig[i].getWidth() * scaleLarge),
					(int) (imagesOrig[i].getHeight() * scaleLarge), true, true);
			imagesLarge[i] = ImageUtil
					.imageColor(imagesLargeWhite[i], 0x000000);
			imagesSmall[i] = ImageUtils.resize(imagesOrig[i],
					(int) (imagesOrig[i].getWidth() * scaleSmall),
					(int) (imagesOrig[i].getHeight() * scaleSmall), true, true);
		}
		fontLargeWhite = new Font(imagesLargeWhite, charset, -3);
		fontLarge = new Font(imagesLarge, charset, -3);
		fontSmall = new Font(imagesSmall, charset, -3);
	}

	private String makeTimeString(int t) {
		int seconds = (int) Math.ceil(((double) t) * timeQuantum / 1000);
		int m = seconds / 60;
		int s = seconds % 60;
		int h = m / 60;
		m %= 60;
		int m1 = m / 10;
		int m2 = m % 10;
		int s1 = s / 10;
		int s2 = s % 10;
		return h + ":" + m1 + m2 + ":" + s1 + s2;
	}

	private String makeTimeStringShort(int t) {
		int seconds = (int) Math.ceil(((double) t) * timeQuantum / 1000);
		int m = seconds / 60;
		int s = seconds % 60;
		int h = m / 60;
		m %= 60;
		int m1 = m / 10;
		int m2 = m % 10;
		int s1 = s / 10;
		int s2 = s % 10;
		if (h == 0)
			return m + ":" + s1 + s2;
		else
			return h + ":" + m1 + m2 + ":" + s1 + s2;
	}

	public void paint(Graphics g) {
		super.paint(g);

		updateDimensions();
		if (displayState == 0)
			paintClocks(g);
		else if (displayState == 1)
			paintSettings(g);
		else if (displayState == 2)
			paintTimeEdit(g);
		else if (displayState == 3)
			paintIntro(g);
	}

	private void paintIntro(Graphics g) {
		g.setColor(backColor);
		g.fillRect(0, 0, width, height);

		String title1 = "Touch";
		String title2 = "Clock";
		fontLargeWhite.write(g, title1,
				width / 2 - fontLargeWhite.getWidth(title1) / 2,
				settingSpace * 3, 0, 0, Component.ALIGN_TOP_LEFT);
		fontLargeWhite.write(g, title2,
				width / 2 - fontLargeWhite.getWidth(title2) / 2, settingSpace
						* 3 + fontLargeWhite.getHeight(), 0, 0,
				Component.ALIGN_TOP_LEFT);

		String author = "by Francois van Niekerk";
		fontSmall.write(g, author, width / 2 - fontSmall.getWidth(author) / 2,
				settingSpace * 6 + fontLargeWhite.getHeight() * 2, 0, 0,
				Component.ALIGN_TOP_LEFT);

		g.drawImage(playImageNavi, width / 2 - naviButtonSize / 2, height
				- naviButtonSize - settingSpace, Graphics.TOP | Graphics.LEFT);
		g.drawImage(closeImageNavi, width / 2 + naviOffset, height
				- naviButtonSize - settingSpace, Graphics.TOP
				| Graphics.HCENTER);
		if (!soundOn)
			g.drawImage(muteImageNavi, width / 2 - naviOffset, height
					- naviButtonSize - settingSpace, Graphics.TOP
					| Graphics.HCENTER);
		else
			g.drawImage(soundImageNavi, width / 2 - naviOffset, height
					- naviButtonSize - settingSpace, Graphics.TOP
					| Graphics.HCENTER);
	}

	private void paintTimeEdit(Graphics g) {
		g.setColor(backColor);
		g.fillRect(0, 0, width, height);

		String title = "Edit Time";
		fontLargeWhite.write(g, title,
				width / 2 - fontLargeWhite.getWidth(title) / 2, settingSpace,
				0, 0, Component.ALIGN_TOP_LEFT);

		String timeString = makeTimeString(editTime * 1000 / timeQuantum);
		fontLargeWhite.write(g, timeString,
				width / 2 - fontLargeWhite.getWidth(timeString) / 2, height / 2
						- fontLargeWhite.getHeight() / 2, 0, 0,
				Component.ALIGN_TOP_LEFT);

		editDigits = makeTimeString(0).length();
		editWidth = fontLargeWhite.getWidth(makeTimeString(0));
		editDigitWidth = editWidth / editDigits;
		g.drawImage(plusImageSetting, width / 2 - editWidth / 2, height / 2
				- fontLargeWhite.getHeight() / 2 - settingSpace
				- settingButtonSize, Graphics.TOP | Graphics.LEFT);
		g.drawImage(plusImageSetting, width / 2 - editWidth / 2
				+ editDigitWidth * 2, height / 2 - fontLargeWhite.getHeight()
				/ 2 - settingSpace - settingButtonSize, Graphics.TOP
				| Graphics.LEFT);
		g.drawImage(plusImageSetting, width / 2 - editWidth / 2
				+ editDigitWidth * 3, height / 2 - fontLargeWhite.getHeight()
				/ 2 - settingSpace - settingButtonSize, Graphics.TOP
				| Graphics.LEFT);
		g.drawImage(plusImageSetting, width / 2 - editWidth / 2
				+ editDigitWidth * 5, height / 2 - fontLargeWhite.getHeight()
				/ 2 - settingSpace - settingButtonSize, Graphics.TOP
				| Graphics.LEFT);
		g.drawImage(plusImageSetting, width / 2 - editWidth / 2
				+ editDigitWidth * 6, height / 2 - fontLargeWhite.getHeight()
				/ 2 - settingSpace - settingButtonSize, Graphics.TOP
				| Graphics.LEFT);
		g.drawImage(minusImageSetting, width / 2 - editWidth / 2, height / 2
				+ fontLargeWhite.getHeight() / 2 + settingSpace, Graphics.TOP
				| Graphics.LEFT);
		g.drawImage(minusImageSetting, width / 2 - editWidth / 2
				+ editDigitWidth * 2, height / 2 + fontLargeWhite.getHeight()
				/ 2 + settingSpace, Graphics.TOP | Graphics.LEFT);
		g.drawImage(minusImageSetting, width / 2 - editWidth / 2
				+ editDigitWidth * 3, height / 2 + fontLargeWhite.getHeight()
				/ 2 + settingSpace, Graphics.TOP | Graphics.LEFT);
		g.drawImage(minusImageSetting, width / 2 - editWidth / 2
				+ editDigitWidth * 5, height / 2 + fontLargeWhite.getHeight()
				/ 2 + settingSpace, Graphics.TOP | Graphics.LEFT);
		g.drawImage(minusImageSetting, width / 2 - editWidth / 2
				+ editDigitWidth * 6, height / 2 + fontLargeWhite.getHeight()
				/ 2 + settingSpace, Graphics.TOP | Graphics.LEFT);

		g.drawImage(okImageNavi, width / 2 - naviButtonSize / 2, height
				- naviButtonSize - settingSpace, Graphics.TOP | Graphics.LEFT);
		g.drawImage(closeImageNavi, width / 2 + naviOffset, height
				- naviButtonSize - settingSpace, Graphics.TOP
				| Graphics.HCENTER);
	}

	private void paintSettings(Graphics g) {
		g.setColor(backColor);
		g.fillRect(0, 0, width, height);

		String title = "Settings";
		fontLargeWhite.write(g, title,
				width / 2 - fontLargeWhite.getWidth(title) / 2, settingSpace,
				0, 0, Component.ALIGN_TOP_LEFT);

		fontSmall.write(g, "Main Time: "
				+ makeTimeString(settingMainTime * 1000 / timeQuantum),
				settingSpace, settingSpace * 2 + fontLargeWhite.getHeight(), 0,
				0, Component.ALIGN_TOP_LEFT);
		g.drawImage(spannerImageSetting, width - settingButtonSize
				- settingSpace, settingSpace * 2 + fontLargeWhite.getHeight(),
				Graphics.TOP | Graphics.LEFT);

		String overtimeNice = "Absolute";
		if (settingOvertimeType == 1)
			overtimeNice = "Japanese Byoyomi";
		else if (settingOvertimeType == 2)
			overtimeNice = "Canadian Overtime";
		fontSmall.write(g, overtimeNice, settingSpace, settingSpace * 3
				+ fontLargeWhite.getHeight() + fontSmall.getHeight(), 0, 0,
				Component.ALIGN_TOP_LEFT);
		g.drawImage(spannerImageSetting, width - settingButtonSize
				- settingSpace, settingSpace * 3 + fontLargeWhite.getHeight()
				+ fontSmall.getHeight(), Graphics.TOP | Graphics.LEFT);

		if (settingOvertimeType == 1) {
			fontSmall.write(g, "Periods: " + settingOvertimeUnits,
					settingSpace, settingSpace * 4 + fontLargeWhite.getHeight()
							+ fontSmall.getHeight() * 2, 0, 0,
					Component.ALIGN_TOP_LEFT);
			g.drawImage(
					plusImageSetting,
					width - settingButtonSize - settingSpace,
					settingSpace * 4 + fontLargeWhite.getHeight()
							+ fontSmall.getHeight() * 2, Graphics.TOP
							| Graphics.LEFT);
			g.drawImage(
					minusImageSetting,
					width - settingButtonSize * 2 - settingSpace * 2,
					settingSpace * 4 + fontLargeWhite.getHeight()
							+ fontSmall.getHeight() * 2, Graphics.TOP
							| Graphics.LEFT);
			fontSmall.write(g, "Period: "
					+ makeTimeStringShort(settingOvertimeTime * 1000
							/ timeQuantum), settingSpace, settingSpace * 5
					+ fontLargeWhite.getHeight() + fontSmall.getHeight() * 3,
					0, 0, Component.ALIGN_TOP_LEFT);
			g.drawImage(
					spannerImageSetting,
					width - settingButtonSize - settingSpace,
					settingSpace * 5 + fontLargeWhite.getHeight()
							+ fontSmall.getHeight() * 3, Graphics.TOP
							| Graphics.LEFT);
		} else if (settingOvertimeType == 2) {
			fontSmall.write(
					g,
					"Stones: " + settingOvertimeUnits,
					settingSpace,
					settingSpace * 4 + fontLargeWhite.getHeight()
							+ fontSmall.getHeight() * 2, 0, 0,
					Component.ALIGN_TOP_LEFT);
			g.drawImage(
					plusImageSetting,
					width - settingButtonSize - settingSpace,
					settingSpace * 4 + fontLargeWhite.getHeight()
							+ fontSmall.getHeight() * 2, Graphics.TOP
							| Graphics.LEFT);
			g.drawImage(
					minusImageSetting,
					width - settingButtonSize * 2 - settingSpace * 2,
					settingSpace * 4 + fontLargeWhite.getHeight()
							+ fontSmall.getHeight() * 2, Graphics.TOP
							| Graphics.LEFT);
			fontSmall.write(g, "Time: "
					+ makeTimeStringShort(settingOvertimeTime * 1000
							/ timeQuantum), settingSpace, settingSpace * 5
					+ fontLargeWhite.getHeight() + fontSmall.getHeight() * 3,
					0, 0, Component.ALIGN_TOP_LEFT);
			g.drawImage(
					spannerImageSetting,
					width - settingButtonSize - settingSpace,
					settingSpace * 5 + fontLargeWhite.getHeight()
							+ fontSmall.getHeight() * 3, Graphics.TOP
							| Graphics.LEFT);
		}

		g.drawImage(okImageNavi, width / 2 - naviButtonSize / 2, height
				- naviButtonSize - settingSpace, Graphics.TOP | Graphics.LEFT);
		g.drawImage(closeImageNavi, width / 2 + naviOffset, height
				- naviButtonSize - settingSpace, Graphics.TOP
				| Graphics.HCENTER);
		if (!soundOn)
			g.drawImage(muteImageNavi, width / 2 - naviOffset, height
					- naviButtonSize - settingSpace, Graphics.TOP
					| Graphics.HCENTER);
		else
			g.drawImage(soundImageNavi, width / 2 - naviOffset, height
					- naviButtonSize - settingSpace, Graphics.TOP
					| Graphics.HCENTER);
	}

	private void paintClocks(Graphics g) {
		g.setColor(backColor);
		g.fillRect(0, 0, width, height);

		Image lowbarLowerImage = Image.createImage(width, lowerBarSize);
		Image lowbarUpperImage = Image.createImage(width, lowerBarSize);
		lowbarLowerImage.getGraphics().drawImage(lowbarImageResized, 0, 0,
				Graphics.TOP | Graphics.LEFT);
		lowbarUpperImage.getGraphics().drawImage(lowbarImageResized, 0, 0,
				Graphics.TOP | Graphics.LEFT);
		lowbarLowerImage.getGraphics().drawImage(swapImageResized,
				lowerBarSpace,
				lowerBarSize / 2 - swapImageResized.getHeight() / 2,
				Graphics.TOP | Graphics.LEFT);
		fontSmall.write(lowbarLowerImage.getGraphics(), movesLower + "",
				lowerBarSpace * 2 + lowerBarButtonSize, lowerBarSize / 2
						- fontSmall.getHeight() / 2, 0, 0,
				Component.ALIGN_TOP_LEFT);
		lowbarUpperImage.getGraphics().drawImage(swapImageResized,
				lowerBarSpace,
				lowerBarSize / 2 - swapImageResized.getHeight() / 2,
				Graphics.TOP | Graphics.LEFT);
		fontSmall.write(lowbarUpperImage.getGraphics(), movesUpper + "",
				lowerBarSpace * 2 + lowerBarButtonSize, lowerBarSize / 2
						- fontSmall.getHeight() / 2, 0, 0,
				Component.ALIGN_TOP_LEFT);
		int timeNiceWidth = fontSmall.getWidth(timeNice);
		fontSmall.write(lowbarLowerImage.getGraphics(), timeNice, width
				- timeNiceWidth - lowerBarSpace,
				lowerBarSize / 2 - fontSmall.getHeight() / 2, 0, 0,
				Component.ALIGN_TOP_LEFT);
		fontSmall.write(lowbarUpperImage.getGraphics(), timeNice, width
				- timeNiceWidth - lowerBarSpace,
				lowerBarSize / 2 - fontSmall.getHeight() / 2, 0, 0,
				Component.ALIGN_TOP_LEFT);
		lowbarLowerImage.getGraphics().drawImage(clockImageResized,
				width - timeNiceWidth - lowerBarSpace * 2 - lowerBarButtonSize,
				lowerBarSize / 2 - swapImageResized.getHeight() / 2,
				Graphics.TOP | Graphics.LEFT);
		lowbarUpperImage.getGraphics().drawImage(clockImageResized,
				width - timeNiceWidth - lowerBarSpace * 2 - lowerBarButtonSize,
				lowerBarSize / 2 - swapImageResized.getHeight() / 2,
				Graphics.TOP | Graphics.LEFT);

		g.drawImage(lowbarLowerImage, 0, height - lowerBarSize, Graphics.TOP
				| Graphics.LEFT);
		g.drawRegion(lowbarUpperImage, 0, 0, lowbarUpperImage.getWidth(),
				lowbarUpperImage.getHeight(), Sprite.TRANS_ROT180, 0, 0,
				Graphics.TOP | Graphics.LEFT);
		g.drawImage(midbarImageResized, width / 2, height / 2, Graphics.VCENTER
				| Graphics.HCENTER);

		if (!running)
			g.drawImage(pauseImageResized, width / 2, height / 2,
					Graphics.VCENTER | Graphics.HCENTER);
		else
			g.drawImage(playImageResized, width / 2, height / 2,
					Graphics.VCENTER | Graphics.HCENTER);
		g.drawImage(closeImageResized, width / 2 + midBarOffset, height / 2,
				Graphics.VCENTER | Graphics.HCENTER);
		// g.drawImage(infoImageResized, width / 2 - midBarOffset, height / 2,
		// Graphics.VCENTER | Graphics.HCENTER);
		if (!soundOn)
			g.drawImage(muteImageResized, width / 2 - midBarOffset, height / 2,
					Graphics.VCENTER | Graphics.HCENTER);
		else
			g.drawImage(soundImageResized, width / 2 - midBarOffset,
					height / 2, Graphics.VCENTER | Graphics.HCENTER);

		String timeLowerString = "?";
		if (timeLeftLower > 0 || overtimeType == 0)
			timeLowerString = makeTimeString(timeLeftLower);
		else if (overtimeType == 1 || overtimeType == 2)
			timeLowerString = makeTimeStringShort(overtimeTimeLower) + " ("
					+ overtimeUnitsLower + ")";
		String timeUpperString = "?";
		if (timeLeftUpper > 0 || overtimeType == 0)
			timeUpperString = makeTimeString(timeLeftUpper);
		else if (overtimeType == 1 || overtimeType == 2)
			timeUpperString = makeTimeStringShort(overtimeTimeUpper) + " ("
					+ overtimeUnitsUpper + ")";
		int timeWidthLower = fontLarge.getWidth(timeLowerString);
		int timeWidthUpper = fontLarge.getWidth(timeUpperString);
		int timeHeight = fontLarge.getHeight();

		Image lowerButtonImage = Image.createImage(mainButtonWidth,
				mainButtonHeight);
		Image upperButtonImage = Image.createImage(mainButtonWidth,
				mainButtonHeight);

		if ((timeLeftLower == 0 && overtimeType == 0)
				|| (overtimeType == 1 && overtimeUnitsLower == 0)
				|| (overtimeType == 2 && overtimeTimeLower == 0))
			lowerButtonImage.getGraphics().drawImage(buttonAlertImageResized,
					0, 0, Graphics.TOP | Graphics.LEFT);
		else if (runningLower)
			lowerButtonImage.getGraphics().drawImage(buttonRunningImageResized,
					0, 0, Graphics.TOP | Graphics.LEFT);
		else
			lowerButtonImage.getGraphics().drawImage(buttonImageResized, 0, 0,
					Graphics.TOP | Graphics.LEFT);
		if ((timeLeftUpper == 0 && overtimeType == 0)
				|| (overtimeType == 1 && overtimeUnitsUpper == 0)
				|| (overtimeType == 2 && overtimeTimeUpper == 0))
			upperButtonImage.getGraphics().drawImage(buttonAlertImageResized,
					0, 0, Graphics.TOP | Graphics.LEFT);
		else if (runningUpper)
			upperButtonImage.getGraphics().drawImage(buttonRunningImageResized,
					0, 0, Graphics.TOP | Graphics.LEFT);
		else
			upperButtonImage.getGraphics().drawImage(buttonImageResized, 0, 0,
					Graphics.TOP | Graphics.LEFT);

		fontLarge.write(lowerButtonImage.getGraphics(), timeLowerString,
				mainButtonWidth / 2 - timeWidthLower / 2, mainButtonHeight / 2
						- timeHeight / 2, 0, 0, Component.ALIGN_TOP_LEFT);
		fontLarge.write(upperButtonImage.getGraphics(), timeUpperString,
				mainButtonWidth / 2 - timeWidthUpper / 2, mainButtonHeight / 2
						- timeHeight / 2, 0, 0, Component.ALIGN_TOP_LEFT);
		g.drawImage(lowerButtonImage, width / 2 - mainButtonWidth / 2, height
				/ 2 + mainButtonVertOffset - mainButtonHeight / 2, Graphics.TOP
				| Graphics.LEFT);
		g.drawRegion(upperButtonImage, 0, 0, lowerButtonImage.getWidth(),
				lowerButtonImage.getHeight(), Sprite.TRANS_ROT180, width / 2
						- mainButtonWidth / 2, height / 2
						- mainButtonVertOffset - mainButtonHeight / 2,
				Graphics.TOP | Graphics.LEFT);
	}

	public void commandAction(Command command, Displayable displayable) {
	}

	protected void pointerPressed(int x, int y) {
		if (displayState == 0) {
			if (y >= (height / 2 - midBarSize / 2)
					&& y <= (height / 2 + midBarSize / 2)) { // midbar
				if (Math.abs(width / 2 - x) <= midBarButtonSize) { // center
					if (!someoneLost) {
						running = !running;
						repaint();
						playBeep();
					}
				} else if (Math.abs(width / 2 - midBarOffset - x) <= midBarButtonSize) { // left
					soundOn = !soundOn;
					playClick();
					repaint();
				} else if (Math.abs(width / 2 + midBarOffset - x) <= midBarButtonSize) { // right
					running = false;
					displayState = 1;
					playClick();
					repaint();
				}

			} else if (y <= lowerBarSize) {// topbar
			} else if (y >= (height - lowerBarSize)) { // bottombar
			} else if (x >= (width / 2 - mainButtonWidth / 2)
					&& x <= (width / 2 + mainButtonWidth / 2)) {
				if (y >= (height / 2 - mainButtonVertOffset - mainButtonWidth / 2)
						&& y <= (height / 2 - mainButtonVertOffset + mainButtonWidth / 2)) { // uppermain
					if (running && !someoneLost && runningUpper) {
						runningUpper = false;
						runningLower = true;
						movesUpper++;
						if (overtimeType == 1)
							overtimeTimeUpper = overtimeTime;
						else if (overtimeType == 2 && timeLeftUpper == 0) {
							overtimeUnitsUpper--;
							if (overtimeUnitsUpper == 0) {
								overtimeUnitsUpper = overtimeUnits;
								overtimeTimeUpper = overtimeTime;
								playBeep();
							}
						}
						playClick();
					}
				} else if (y >= (height / 2 + mainButtonVertOffset - mainButtonWidth / 2)
						&& y <= (height / 2 + mainButtonVertOffset + mainButtonWidth / 2)) { // lowermain
					if (running && !someoneLost && runningLower) {
						runningLower = false;
						runningUpper = true;
						movesLower++;
						if (overtimeType == 1)
							overtimeTimeLower = overtimeTime;
						else if (overtimeType == 2 && timeLeftLower == 0) {
							overtimeUnitsLower--;
							if (overtimeUnitsLower == 0) {
								overtimeUnitsLower = overtimeUnits;
								overtimeTimeLower = overtimeTime;
								playBeep();
							}
						}
						playClick();
					}
				}
			}
		} else if (displayState == 1) {
			if (y >= (height - naviButtonSize - settingSpace)) { // lower area
				if (Math.abs(width / 2 - x) <= naviButtonSize) { // center
					setClock(settingMainTime, settingOvertimeType,
							settingOvertimeUnits, settingOvertimeTime);
					displayState = 0;
					playClick();
					repaint();
				} else if (Math.abs(width / 2 + naviOffset - x) <= naviButtonSize) { // right
					displayState = 3;
					playClick();
					repaint();
				} else if (Math.abs(width / 2 - naviOffset - x) <= naviButtonSize) { // right
					soundOn = !soundOn;
					playClick();
					repaint();
				}
			} else if (x >= (width - settingButtonSize * 2 - settingSpace * 2)) { // setting
																					// area
				int gridx = (x - (width - settingButtonSize * 2 - settingSpace * 2))
						/ (settingButtonSize + settingSpace);
				int gridy = (y - (fontLargeWhite.getHeight() + settingSpace * 2))
						/ (settingButtonSize + settingSpace);

				if (gridx == 1 && gridy == 0) {
					editTime = settingMainTime;
					editType = 0;
					displayState = 2;
					playClick();
					repaint();
				} else if (gridx == 1 && gridy == 1) {
					settingOvertimeType++;
					if (settingOvertimeType > 2)
						settingOvertimeType = 0;

					if (settingOvertimeType == 1) {
						settingOvertimeUnits = 5;
						settingOvertimeTime = 30;
					} else if (settingOvertimeType == 2) {
						settingOvertimeUnits = 25;
						settingOvertimeTime = 600;
					}
					playClick();
					repaint();
				} else if (gridx == 0 && gridy == 2) {
					settingOvertimeUnits--;
					if (settingOvertimeUnits < 1)
						settingOvertimeUnits = 1;
					playClick();
					repaint();
				} else if (gridx == 1 && gridy == 2) {
					settingOvertimeUnits++;
					playClick();
					repaint();
				} else if (gridx == 1 && gridy == 3) {
					editTime = settingOvertimeTime;
					editType = 1;
					displayState = 2;
					playClick();
					repaint();
				}
			}
		} else if (displayState == 2) {
			if (y >= (height - naviButtonSize - settingSpace)) { // lower area
				if (Math.abs(width / 2 - x) <= naviButtonSize) { // center
					if (editType == 0)
						settingMainTime = editTime;
					else if (editType == 1)
						settingOvertimeTime = editTime;
					displayState = 1;
					playClick();
					repaint();
				} else if (Math.abs(width / 2 + naviOffset - x) <= naviButtonSize) { // right
					displayState = 1;
					playClick();
					repaint();
				}
			} else {
				int op = 0;
				if (Math.abs(height / 2 - fontLargeWhite.getHeight() / 2
						- settingSpace - settingButtonSize / 2 - y) <= settingButtonSize) {
					op = +1;
				} else if (Math.abs(height / 2 + fontLargeWhite.getHeight() / 2
						+ settingSpace + settingButtonSize / 2 - y) <= settingButtonSize) {
					op = -1;
				}
				if (op != 0) {
					int digit = (x - (width / 2 - editWidth / 2))
							/ (editDigitWidth);

					if (digit == 6) {
						editTime += op;
					} else if (digit == 5) {
						if (op > 0 || editTime >= 10)
							editTime += op * 10;
					} else if (digit == 3) {
						if (op > 0 || editTime >= 60)
							editTime += op * 60;
					} else if (digit == 2) {
						if (op > 0 || editTime >= 600)
							editTime += op * 600;
					} else if (digit == 0) {
						if (op > 0 || editTime >= 3600)
							editTime += op * 3600;
					}

					if (editTime < 0)
						editTime = 0;
					else if (editTime > (60 * 60 * 10 - 1))
						editTime = 60 * 60 * 10 - 1;
					playClick();
					repaint();
				}
			}
		} else if (displayState == 3) {
			if (y >= (height - naviButtonSize - settingSpace)) { // lower area
				if (Math.abs(width / 2 - x) <= naviButtonSize) { // center
					displayState = 1;
					playClick();
					repaint();
				} else if (Math.abs(width / 2 + naviOffset - x) <= naviButtonSize) { // right
					midlet.exit();
				} else if (Math.abs(width / 2 - naviOffset - x) <= naviButtonSize) { // right
					soundOn = !soundOn;
					playClick();
					repaint();
				}
			}
		}
	}

	private void playClick() {
		if (soundOn) {
			try {
				clickPlayer.stop();
				clickPlayer.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void playBeep() {
		if (soundOn) {
			try {
				beepPlayer.stop();
				beepPlayer.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void setClock(int mainTime) {
		setClock(mainTime, 0, 0, 0);
	}

	public void setClock(int mainTime, int overtimeType, int overtimeUnits,
			int overtimeTime) {
		timeLeftLower = mainTime * 1000 / timeQuantum;
		timeLeftUpper = mainTime * 1000 / timeQuantum;

		this.overtimeType = overtimeType;
		this.overtimeUnits = overtimeUnits;
		overtimeUnitsLower = overtimeUnits;
		overtimeUnitsUpper = overtimeUnits;
		this.overtimeTime = overtimeTime * 1000 / timeQuantum;
		overtimeTimeLower = overtimeTime * 1000 / timeQuantum;
		overtimeTimeUpper = overtimeTime * 1000 / timeQuantum;

		if (overtimeType == 1) // japanese
			timeNice = makeTimeString(timeLeftLower) + "+" + overtimeUnits
					+ "x" + makeTimeStringShort(overtimeTimeLower);
		else if (overtimeType == 2) // canadian
			timeNice = makeTimeString(timeLeftLower) + "+" + overtimeUnits
					+ "/" + makeTimeStringShort(overtimeTimeLower);
		else
			timeNice = makeTimeString(timeLeftLower);

		movesLower = 0;
		movesUpper = 0;

		running = false;
		runningLower = true;
		runningUpper = false;
		someoneLost = false;

		repaint();
	}

	private class MyTimerTask extends TimerTask {

		ClockDisplay parent;

		MyTimerTask(ClockDisplay parent) {
			this.parent = parent;
		}

		public final void run() {
			if (parent.running) {
				if (parent.runningLower) {
					if (parent.timeLeftLower > 0) {
						parent.timeLeftLower--;
						if (timeLeftLower == soundMainTimeWarning)
							playBeep();
						if (timeLeftLower == soundFinalWarning
								&& overtimeType == 0)
							playBeep();
						if (parent.timeLeftLower == 0) {
							someoneLost = (parent.overtimeType == 0);
							playBeep();
						}
					} else if (overtimeType == 1) {// japanese
						if (overtimeTimeLower > 0) {
							overtimeTimeLower--;
							if (overtimeTimeLower == soundFinalWarning)
								playBeep();
							if (overtimeTimeLower == 0) {
								overtimeUnitsLower--;
								playBeep();
								if (overtimeUnitsLower == 0) {
									someoneLost = true;
								} else
									overtimeTimeLower = overtimeTime;
							}
						}
					} else if (overtimeType == 2) {// canadian
						if (overtimeTimeLower > 0) {
							overtimeTimeLower--;
							if (overtimeTimeLower == soundFinalWarning)
								playBeep();
							if (overtimeTimeLower == 0) {
								someoneLost = true;
								playBeep();
							}
						}
					}
				}
				if (parent.runningUpper) {
					if (parent.timeLeftUpper > 0) {
						parent.timeLeftUpper--;
						if (timeLeftUpper == soundMainTimeWarning)
							playBeep();
						if (timeLeftUpper == soundFinalWarning
								&& overtimeType == 0)
							playBeep();
						if (parent.timeLeftUpper == 0) {
							someoneLost = (parent.overtimeType == 0);
							playBeep();
						}
					} else if (overtimeType == 1) {// japanese
						if (overtimeTimeUpper > 0) {
							overtimeTimeUpper--;
							if (overtimeTimeUpper == soundFinalWarning)
								playBeep();
							if (overtimeTimeUpper == 0) {
								overtimeUnitsUpper--;
								playBeep();
								if (overtimeUnitsUpper == 0) {
									someoneLost = true;
								} else
									overtimeTimeUpper = overtimeTime;
							}
						}
					} else if (overtimeType == 2) {// canadian
						if (overtimeTimeUpper > 0) {
							overtimeTimeUpper--;
							if (overtimeTimeUpper == soundFinalWarning)
								playBeep();
							if (overtimeTimeUpper == 0) {
								someoneLost = true;
								playBeep();
							}
						}
					}
				}
				parent.repaint();
			}
		}

	}
}
