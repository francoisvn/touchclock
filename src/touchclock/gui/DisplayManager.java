package touchclock.gui;

import java.util.Stack;

import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;

public class DisplayManager {

	private Displayable current;
	private Display display;
	private Stack stack;

	public DisplayManager(Display display) {
		if (display == null) {
			throw new IllegalArgumentException("Display can not be null.");
		}
		this.display = display;
		this.stack = new Stack();
	}

	public void setDisplayable(Displayable next) {
		if (this.current == next) {
			return;
		}

		if (this.current != null) {
			this.stack.push(this.current);
		}
		this.current = next;
		this.display.setCurrent(this.current);
	}

	public void previousDisplayable() {
		if (this.stack.size() > 0) {
			this.current = (Displayable) this.stack.pop();
			this.display.setCurrent(this.current);
		}
	}

}
