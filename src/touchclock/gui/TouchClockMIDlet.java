package touchclock.gui;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class TouchClockMIDlet extends MIDlet implements CommandListener {

	public DisplayManager manager;
	private ClockDisplay clockDisplay;


	public TouchClockMIDlet() {
		manager = new DisplayManager(Display.getDisplay(this));

		clockDisplay = new ClockDisplay(this);
	}

	protected void startApp() throws MIDletStateChangeException {
		manager.setDisplayable(clockDisplay);
	}

	public void exit() {
		manager.setDisplayable(null);
		destroyApp(true);
		notifyDestroyed();
	}

	public void commandAction(Command command, Displayable displayable) {
	}

	protected void destroyApp(boolean unconditional) {
	}

	protected void pauseApp() {
	}
}
